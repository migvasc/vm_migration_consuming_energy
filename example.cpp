/* Copyright (c) 2007-2020. The SimGrid Team.
 * All rights reserved.                                                     */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */
#include "simgrid/s4u.hpp"
#include "simgrid/plugins/energy.h"
#include "simgrid/s4u/VirtualMachine.hpp"
#include <cmath>

#include "simgrid/plugins/live_migration.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(example, "Messages specific for this example");

static void printEnergyConsumption(){
  XBT_INFO("printEnergyConsumption");
  for (simgrid::s4u::Host *pm : simgrid::s4u::Engine::get_instance()->get_all_hosts() ){
    if(!dynamic_cast<simgrid::s4u::VirtualMachine *>(pm)){      
      XBT_INFO("Host %s, Energy (J) %f",pm->get_cname(),sg_host_get_consumed_energy(pm));
      XBT_INFO("current consumption  %f",sg_host_get_current_consumption(pm));               
    }      
  }
}



class MainActor
{

private:
    int command;

public:
    static void vm_migrate(simgrid::s4u::VirtualMachine* vm, simgrid::s4u::Host* dst_pm)
    {
        XBT_INFO("%s migration started! ", vm->get_cname());
        const simgrid::s4u::Host* src_pm = vm->get_pm();  
        double mig_sta             = simgrid::s4u::Engine::get_clock();  
        sg_host_energy_update_all();
        printEnergyConsumption();
        sg_vm_migrate(vm, dst_pm);
        printEnergyConsumption();
        sg_host_energy_update_all();
        double mig_end = simgrid::s4u::Engine::get_clock();
        XBT_INFO("######## %s migrated: %s->%s in %g s ########", vm->get_cname(), src_pm->get_cname(), dst_pm->get_cname(), mig_end - mig_sta);  
        sg_host_energy_update_all();        
    }

    
    static void compute_0flops()
    {
          XBT_INFO("Compitomg 0 FLOPS");
          simgrid::s4u::this_actor::execute(0);
          simgrid::s4u::this_actor::exec_async(0);
          simgrid::s4u::this_actor::exec_init(0)->start();    
          
    }

    static void vm_commands(simgrid::s4u::VirtualMachine *vm, simgrid::s4u::Host *dest)
    {
        vm->suspend();
        vm->set_pm(dest);
        vm->resume();
    }

    void run_test()
    {
        simgrid::s4u::Host *pm2 = simgrid::s4u::Host::by_name("node-0.2cores.org");
        simgrid::s4u::Host *pm1 = simgrid::s4u::Host::by_name("node-0.1core.org");
        simgrid::s4u::Host *pm4 = simgrid::s4u::Host::by_name("node-0.4cores.org");
        simgrid::s4u::VirtualMachine *vm0;
        int nCores = 1;
        vm0 = new simgrid::s4u::VirtualMachine("VM0", pm4, nCores);
        vm0->set_ramsize(110000000);
        vm0->start();
        simgrid::s4u::this_actor::sleep_for(1);
        switch (command)
        {
        case 1:
            XBT_INFO("VM will be migrated - live migration plugin ");            
            simgrid::s4u::Actor::create("mig_wrk", pm4, vm_migrate, vm0, pm2);
          break;
        case 2:
            XBT_INFO("VM will be suspended, will have its host changed and will be resumed");                        
            vm_commands(vm0,pm2);
            break;
        case 3:            
            simgrid::s4u::Actor::create("c_pm1", pm1, compute_0flops);
            simgrid::s4u::Actor::create("c_pm2", pm2, compute_0flops);
            simgrid::s4u::Actor::create("c_pm4", pm4, compute_0flops);
            break;
        default:
            XBT_INFO("do nothing");                        
            break;
        }


        sg_host_energy_update_all();
        simgrid::s4u::this_actor::sleep_for(5);  
    }        

    explicit MainActor(std::vector<std::string> args){        
        command = std::stoi(args[1] );
    }

    void operator()(){
        run_test();
    }

};


int main(int argc, char* argv[])
{
  simgrid::s4u::Engine e(&argc, argv);
  sg_vm_live_migration_plugin_init();
  sg_host_energy_plugin_init();
  e.register_actor<MainActor>("mainactor");/* Submits VMs */  
  e.load_platform(argv[1]);
  e.load_deployment(argv[2]);
  e.run(); 
  XBT_INFO("Simulation is over");    
  return 0;
}
