# Measuring Energy Consumption of VM execution with migrations

## Scenario:

From what I've read in this [paper](https://www.computer.org/csdl/journal/cc/2018/01/07274701/13rRUy3gn2m) and from the plugin code, the VM migration does not consume energy because there is no CPU overhead of sending/receiving migration data.

Using the cluster_multi.xml platform from the examples of the platforms and considering two homogeneous physical machines (PMs). Those PMs have a speed of 1 gigaflop/s and energy consumption of 1 J/s when using one core and 0 J/s when idle.

I tried a simple test that consisted of migrating a running VM that is not computing anything. I expected it to consume only the energy related to the host's IDLE state (0J). However, after the migration, the destination host consumed energy even if I am not computing any FLOP.

I looked into the live migration plugin code, and at stage 3 of the migration, it suspends the VM execution on the source host and resumes it on the destination host. The host was changed using the set_pm() method. I looked into this method, and it calls an execution of 0 flops. Maybe it left the destination host in an invalid state?

I also tested only calling the set_pm method, and after executing the method, the same behavior happened, the host consumed energy that was not related to the IDLE state.

However, if I execute a computation of 0 flops, this scenario does not happen.

To run the C++  version, it is first necessary to compile the project, and then execute it:

Example:
```
g++ example.cpp -o app /usr/local/lib/libsimgrid.so.3.25 -I /opt/simgrid/include
./app cluster_multi.xml deployment_file.xml
```

## Description of the deployment files:

- deployment_file_idle.xml: Do not compute anything, so remains in the IDLE state, and the energy consumption is 0J as expected.
- deployment_file_migration.xml: Migrates the VM to another host using the live migration plugin. After the migration, the destination host is not IDLE anymore.
- deployment_file_suspend.xml: Uses the set_pm() method to change the VM host. After executing the method, the host is not IDLE anymore.
- deployment_file_compute.xml:  Compute 0 FLOPS in the host, and the host remains IDLE.